import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThemeCakesComponent } from './theme-cakes.component';

describe('ThemeCakesComponent', () => {
  let component: ThemeCakesComponent;
  let fixture: ComponentFixture<ThemeCakesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThemeCakesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThemeCakesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
