import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BakingClassesComponent } from './baking-classes.component';

describe('BakingClassesComponent', () => {
  let component: BakingClassesComponent;
  let fixture: ComponentFixture<BakingClassesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BakingClassesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BakingClassesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
