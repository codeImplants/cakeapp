import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../../services/common.service';
import { faRupeeSign } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-baking-classes',
  templateUrl: './baking-classes.component.html',
  styleUrls: ['./baking-classes.component.scss']
})
export class BakingClassesComponent implements OnInit {

  faRupeeSign = faRupeeSign;
  data;

  constructor(private http: HttpClient, private commonService: CommonService) { }

  ngOnInit(): void {
    this.getBakingClassesData();
  }

  getBakingClassesData(){
    let serviceData;
    this.commonService.getBakingClassesData().subscribe(data => {
      serviceData = data;
      this.data = serviceData.product;
    })
  }


}
