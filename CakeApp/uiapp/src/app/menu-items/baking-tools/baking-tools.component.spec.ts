import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BakingToolsComponent } from './baking-tools.component';

describe('BakingToolsComponent', () => {
  let component: BakingToolsComponent;
  let fixture: ComponentFixture<BakingToolsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BakingToolsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BakingToolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
