import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonService } from '../../services/common.service';
import { faRupeeSign } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-baking-tools',
  templateUrl: './baking-tools.component.html',
  styleUrls: ['./baking-tools.component.scss']
})
export class BakingToolsComponent implements OnInit {

  faRupeeSign = faRupeeSign;
  data;

  constructor(private http: HttpClient, private commonService: CommonService) { }

  ngOnInit(): void {
    this.getBakingToolsData();
  }

  getBakingToolsData(){
    let serviceData;
    this.commonService.getBakingToolsData().subscribe(data => {
      serviceData = data;
      this.data = serviceData.product;
    })
  }


}
