import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CakeIngridientsComponent } from './cake-ingridients.component';

describe('CakeIngridientsComponent', () => {
  let component: CakeIngridientsComponent;
  let fixture: ComponentFixture<CakeIngridientsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CakeIngridientsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CakeIngridientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
