import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';
import { CommonService } from '../services/common.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  faChevronDown = faChevronDown;
  menus: any;
  subMenus;
  themCake = false
  data;
  isHomeMenu: boolean;
  isCakeMenu: boolean;
  isThemeCakes: boolean;
  isCakeIngridients: boolean;
  isBakingTools: boolean;
  isBakingClasses: boolean;
  
  constructor(private http: HttpClient, private commonService: CommonService) { }

  ngOnInit(): void {
    this.getMenuItemData()
  }

  getMenuItemData(){
    this.commonService.getMenuItemData().subscribe(data => {
      this.data = data;
      console.log('this.data', this.data)
      this.setSubMenu(data);

    })
  }

  setSubMenu(data){
    this.subMenus = this.data.mainMenu;
    console.log('subMenus', this.subMenus)
  }


  showMenu(menu){
    console.log('menu', menu)
    // let filteredData = this.data.filter(element => console.log(element));
    this.data.mainMenu.forEach(element => {
      console.log(element)
      
    });
    
    switch (menu.menuID) {
      case 'H':
        this.isHomeMenu = true
        this.isCakeMenu = false;
        this.isThemeCakes = false;
        this.isCakeIngridients = false;
        this.isBakingTools = false;
        this.isBakingClasses = false;
        break;
      case 'C':
        this.isHomeMenu = false
        this.isCakeMenu = true;
        this.isThemeCakes = false;
        this.isCakeIngridients = false;
        this.isBakingTools = false;
        this.isBakingClasses = false;
        break;
      case 'TC':
        this.isHomeMenu = false
        this.isCakeMenu = false;
        this.isThemeCakes = true;
        this.isCakeIngridients = false;
        this.isBakingTools = false;
        this.isBakingClasses = false;
        break;
      case 'CI':
        this.isHomeMenu = false
        this.isCakeMenu = false;
        this.isThemeCakes = false;
        this.isCakeIngridients = true;
        this.isBakingTools = false;
        this.isBakingClasses = false;
        break;
      case 'BT':
        this.isHomeMenu = false
        this.isCakeMenu = false;
        this.isThemeCakes = false;
        this.isCakeIngridients = false;
        this.isBakingTools = true;
        this.isBakingClasses = false;
        break;
      case 'BC':
        this.isHomeMenu = false
        this.isCakeMenu = false;
        this.isThemeCakes = false;
        this.isCakeIngridients = false;
        this.isBakingTools = false;
        this.isBakingClasses = true;
        break;
      default:
        break;
    }
    

  }

  hideMenu(menu){
    this.isHomeMenu = false;
    this.isCakeMenu = false;
    this.isThemeCakes = false;
    this.isCakeIngridients = false;
    this.isBakingTools = false;
    this.isBakingClasses = false;
  
  }


}
