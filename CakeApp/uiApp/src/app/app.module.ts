import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SearchComponent } from './search/search.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MenuComponent } from './menu/menu.component';
import { MenuComponentItems } from './menu-items/menu-items.component';
import { HomeComponent } from './menu-items/home/home.component';
import { CakesComponent } from './menu-items/cakes/cakes.component';
import { ThemeCakesComponent } from './menu-items/theme-cakes/theme-cakes.component';
import { CakeIngridientsComponent } from './menu-items/cake-ingridients/cake-ingridients.component';
import { BakingToolsComponent } from './menu-items/baking-tools/baking-tools.component';
import { BakingClassesComponent } from './menu-items/baking-classes/baking-classes.component';
import { FooterComponent } from './footer/footer.component';
import { HttpClientModule } from '@angular/common/http';
import { ImgCarouselComponent } from './img-carousel/img-carousel.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SearchComponent,
    MenuComponentItems,
    MenuComponent,
    HomeComponent,
    CakesComponent,
    ThemeCakesComponent,
    CakeIngridientsComponent,
    BakingToolsComponent,
    BakingClassesComponent,
    FooterComponent,
    ImgCarouselComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
